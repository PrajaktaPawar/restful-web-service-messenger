RESTful web service implementation using Jersey
A Social Media Application Messenger
1  APIS To GET, UPDATE, DELETE and ADD messages, Comments, Links and Profile Information.
Main purpose of project is to implement RestFul APis and routes Using JAX-RS (Jersey) , So I have not used any database .Instead I have used manual data and stored that to Map and reutrning values from Map for corresponding REST request.
Added Framework For Exception handling by implementing ExceptionMapper of JAX-RS
Implemented HATEOAS (To Send Links In Response) Using URIInfo.
Content-Negotiation is added to support for both JSON and XML format consumption.
Used POSTMAN To test the APIS.